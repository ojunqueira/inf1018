/* Otavio_Junqueira 1113115 */

#include "bigint.h"
#include <stdio.h>

static int numBytes = 6;

void TEST_Create () {
    printf("\n---------- TESTING CREATION ----------\n");
    int i = -5000000000000000;
    
    BigInt a = bi_new(i);
    
    printf("Cria: ");
    dump(&i, 4);
    printf(" = ");
    dump(a, numBytes);
    printf("\n");
}

void TEST_Sum () {
    printf("\n---------- TESTING SUM ---------------\n");
    BigInt a = bi_new(1);
    BigInt b = bi_new(314);
    
    BigInt sum = bi_sum(a, b);
    
    printf("Soma: ");
    dump(a, numBytes);
    printf(" + ");
    dump(b, numBytes);
    printf(" = ");
    dump(sum, numBytes);
    printf("\n");
}

void TEST_Inversion () {
    printf("\n---------- TESTING INVERSION ---------\n");
    BigInt a = bi_new(0);

    BigInt minus = bi_minus(a);
    
    printf("Invr: ");
    dump(a, numBytes);
    printf(" = ");
    dump(minus, numBytes);
    printf("\n");
}

void TEST_Subtraction () {
    printf("\n---------- TESTING SUBTRACTION -------\n");
    BigInt a = bi_new(1);
    BigInt b = bi_new(2);
    
    BigInt sub = bi_sub(a, b);
    printf("Subt: ");
    dump(a, numBytes);
    printf(" - ");
    dump(b, numBytes);
    printf(" = ");
    dump(sub, numBytes);
    printf("\n");
}

void TEST_Multiplication () {
    printf("\n---------- TESTING MULTIPLICATION ----\n");
    BigInt a = bi_new(-5);
    BigInt b = bi_new(10);
    
    BigInt mul = bi_mul(a, b);
    printf("Mult: ");
    dump(a, numBytes);
    printf(" x ");
    dump(b, numBytes);
    printf(" = ");
    dump(mul, numBytes);
    printf("\n");
}

void TEST_ShiftLeft (){
    printf("\n---------- TESTING SHIFT LEFT --------\n");
    BigInt a = bi_new(-4);
    int size = 8;
    
    BigInt shl = bi_shl(a, size);
    printf("Slef: ");
    dump(a, numBytes);
    printf(" (<< %d) = ", size);
    dump(shl, numBytes);
    printf("\n");
}

void TEST_ShiftRight (){
    printf("\n---------- TESTING SHIFT RIGHT -------\n");
    BigInt a = bi_new(-4000);
    int size = 8;
    
    BigInt shr = bi_shr(a, size);
    printf("Srgh: ");
    dump(a, numBytes);
    printf(" (>> %d) = ", size);
    dump(shr, numBytes);
    printf("\n");
}

int main (void)
{
    bi_init(numBytes * 8);
    TEST_Create();
    TEST_Sum();
    TEST_Inversion();
    TEST_Subtraction();
    TEST_Multiplication();
    TEST_ShiftLeft();
    TEST_ShiftRight();
}

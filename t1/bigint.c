/* Otavio_Junqueira 1113115 */

#include "bigint.h"
#include <stdio.h>
#include <stdlib.h>

int nbytes = 0;

static int assert () {
    if (nbytes > 0) {
        return 0;
    }
    printf("\nInicialize a biblioteca antes!");
    return 1;
}

static int is_zero (BigInt a) {
    unsigned char * p_a = a;
    int i = 0;
    for (i = 0; i < nbytes; i++, p_a++) {
        if (*p_a != 0) {
            return 1;
        }
    }
    return 0;
}

void bi_init (int nbits) {
    if (nbits%8 == 0 && nbits > 0) {
        nbytes = nbits/8;
    }
    else {
        printf("\nO número de bits deve ser maior do que zero e divisível por 8!\n");
    }
}

BigInt bi_new (int val) {
    if (assert()) {
        return NULL;
    }
    BigInt a = (BigInt)malloc(nbytes * sizeof(char));
    if(a == NULL){
        printf("\nERRO: Nao foi possivel alocar BigInt");
        return NULL;
    }
    unsigned char * p_val = (unsigned char *) &val;
    unsigned char * p_big = a;
    int i = 1;
    for (i = 1; i <= 4; i++, p_big++, p_val++) {
        *p_big = *p_val;
    }
    if (val < 0) {
        for (i = 5; i <= nbytes; i++, p_big++) {
            *p_big = -1;
        }
    }
    else {
        for (i = 5; i <= nbytes; i++, p_big++) {
            *p_big = 0;
        }
    }
    return a;
}

void bi_destroy (BigInt a) {
    free(a);
}

BigInt bi_sum (BigInt a, BigInt b) {
    if (assert()) {
        return NULL;
    }
    unsigned char * p_a = a;
    unsigned char * p_b = b;
    BigInt big = bi_new(0);
    unsigned char * p_big = big;
    short int s = 0;
    unsigned char * p1_s = (unsigned char *) &s;
    unsigned char * p2_s = p1_s + 1;
    int i;
    for (i = 1; i <= nbytes; i++, p_a++, p_b++, p_big++) {
        s = *p_a + *p_b + *p2_s;
        *p_big = *p1_s;
    }
    return big;
}

BigInt bi_minus (BigInt n) {
    if (assert()) {
        return NULL;
    }
    BigInt big = bi_new(0);
    unsigned char * p_big = n;
    unsigned char * p_aux = big;
    int i;
    for (i = 1; i <= nbytes; i++, p_big++,p_aux++){
        *p_aux=~(*p_big);
    }
    p_aux = big;
    short int s;
    unsigned char * p1_s = (unsigned char *) &s;
    unsigned char * p2_s = p1_s + 1;
    s = *p_aux + 1;
    *p_aux = *p1_s;
    p_aux++;
    for (i = 2; i<= nbytes; i++, p_aux++) {
        s = *p_aux + *p2_s;
        *p_aux = *p1_s;
    }
    return big;
}

BigInt bi_sub (BigInt a, BigInt b) {
    if (assert()) {
        return NULL;
    }
    return bi_sum(a, bi_minus(b));
}

BigInt bi_mul (BigInt a, BigInt b) {
    if (assert()) {
        return NULL;
    }
    BigInt big = bi_new(0);
    while (is_zero(b)) {
        short int s = 0;
        unsigned char * p1_s = (unsigned char *) &s;
        unsigned char * p2_s = p1_s + 1;
        *p2_s = *b;
        s = s >> 1;
        *p1_s = *p1_s >> 7;
        if (*p1_s & 1) {
            big = bi_sum(big, a);
        }
        a = bi_shl(a, 1);
        b = bi_shr(b, 1);
    }
    return big;
}

BigInt bi_shr (BigInt a, int n) {
    if (assert()) {
        return NULL;
    }
    
    short int s = 0;
    unsigned char * p1_s = (unsigned char *) &s;
    unsigned char * p2_s = p1_s + 1;
    unsigned char * p_a = a + nbytes - 1;
    *p1_s = *p_a;
    s = s << 1;
    BigInt big;
    if (*p2_s) {
        big = bi_new(-1);
    }
    else {
        big = bi_new(0);
    }
    unsigned char * p_big = big + ((nbytes - 1) - (n/8));
    p1_s = (unsigned char *) &s;
    p2_s = p1_s + 1;
    int i;
    *p1_s = *p_a;
    s = s >> n%8;
    *p_big = *p1_s;
    p_a--;
    p_big--;
    for (i = (nbytes - n/8); i >= 1; i--, p_big--, p_a--) {
        *p2_s = *(p_a + 1);
        *p1_s = *p_a;
        s = s >> n%8;
        *p_big = *p1_s;
    }
    return big;
}

BigInt bi_shl (BigInt a, int n) {
    if (assert()) {
        return NULL;
    }
    unsigned char * p_a = a;
    BigInt big = bi_new(0);
    unsigned char * p_big = big + (n/8);
    short int s = 0;
    unsigned char * p1_s = (unsigned char *) &s;
    unsigned char * p2_s = p1_s + 1;
    int i;
    *p2_s = *p_a;
    s = s << n%8;
    *p_big = *p2_s;
    p_a++;
    p_big++;
    for (i = n/8 + 2; i <= nbytes; i++, p_big++, p_a++) {
        *p1_s = *(p_a - 1);
        *p2_s = *p_a;
        s = s << n%8;
        *p_big = *p2_s;
    }
    return big;
}

void dump (void * p, int n) {
    unsigned char * p1 = p + n - 1;
    while (n--) {
        printf("%02X:", *p1);
        p1--;
    }
}

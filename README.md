INF 1018
=======

PUC-Rio - 2012.2 - INF1018 - Software Basico

Professor: Ana Lúcia de Moura

Aluno: Otávio Junqueira C. Leão (otaviojcl@me.com)


Etapas
=======

Trabalho 1 - Manipulação de inteiros grandes

Trabalho 2 - Gerador de funções

Descrição dos trabalhos disponível em 'Downloads'.


Requisitos
=======

GCC, the GNU Compiler Collection (https://gcc.gnu.org/)

/* Otavio_Junqueira 111311-5 3WB */

#include <stdlib.h>
#include <stdio.h>
#include "gera_func.h"

void* gera_func (void* f, int n, Parametro params[]){
    unsigned char *code;    // ponteiro para vetor de alocacao do codigo
    int idx = 0;            // inteiro com indice para vetor
    int ebp = 4;            // inteiro que representa ebp
	int func_adress;        // inteiro com ponteiro para funcao
	int i;                  // inteiro para uso em 'for'
    int aux_a, aux_b;       // inteiros para uso no tipo double amarrado ou ponteiro
    int *p;                 // ponteiro para uso no tipo double amarrado ou ponteiro
    
    // Aloca o vetor
	code = (unsigned char*)malloc(64 + (16 * n));
    if (code == NULL) {
        printf("\nErro ao alocar memoria!\n");
        return 0;
    }
	// push %ebp
    code[idx] = 0x55;
    idx++;
    // mov %esp, %ebp
	code[idx] = 0x89;
    idx++;
	code[idx] = 0xe5;
    idx++;
    // soma ebp para dar push (8 para double, 4 outros) se tipo nao amarrado
	for (i = 0; i < n; i++) {
        if (params[i].amarrado == 0){
            if (params[i].tipo == DOUBLE_PAR){
                ebp = ebp + 8;
            }
            else {
                ebp = ebp + 4;
            }
        }
	}
    // for varre parametros de tras para frente (empilhar ao contrario)
	for (i = (n - 1); i >= 0; i--){
        // Se parametro amarrado
		if (params[i].amarrado == 1){
            if(params[i].tipo == DOUBLE_PAR){
                p =  (int *)&params[i].valor.v_double;
                aux_a = *p;
                aux_b = *(p + 1);
                code[idx] = 0x68;
                idx++;
                code[idx] = aux_b;
                idx++;
                code[idx] = (aux_b >> 8);
                idx++;
                code[idx] = (aux_b >> 16);
                idx++;
                code[idx] = (aux_b >> 24);
                idx++;
                code[idx] = 0x68;
                idx++;
                code[idx] = aux_a;
                idx++;
                code[idx] = (aux_a >> 8);
                idx++;
                code[idx] = (aux_a >> 16);
                idx++;
                code[idx] = (aux_a >> 24);
                idx++;
			}
			else if (params[i].tipo == INT_PAR){
                code[idx] = 0x68;
                idx++;
                code[idx] = params[i].valor.v_int;
                idx++;
                code[idx] = (params[i].valor.v_int >> 8);
                idx++;
                code[idx] = (params[i].valor.v_int >> 16);
                idx++;
                code[idx] = (params[i].valor.v_int >> 24);
                idx++;
			}
			else if (params[i].tipo == CHAR_PAR){
                code[idx] = 0x68;
                idx++;
                code[idx] = params[i].valor.v_char;
                idx++;
                code[idx] = (params[i].valor.v_char >> 8);
                idx++;
                code[idx] = (params[i].valor.v_char >> 16);
                idx++;
                code[idx] = (params[i].valor.v_char >> 24);
                idx++;
			}
            else if (params[i].tipo == PTR_PAR){
                p =  (int *)&params[i].valor.v_ptr;
                aux_a = *p;
                code[idx] = 0x68;
                idx++;
                code[idx] = aux_a;
                idx++;
                code[idx] = (aux_a >> 8);
                idx++;
                code[idx] = (aux_a >> 16);
                idx++;
                code[idx] = (aux_a >> 24);
                idx++;
			}
		}
        // Parametro nao amarrado
		else if (params[i].amarrado == 0){
            code[idx] = 0xff;
            idx++;
            code[idx] = 0x75;
            idx++;
            code[idx] = (unsigned char)ebp;
            idx++;
            ebp = ebp - 4;
            // Se double, novo push
			if(params[i].tipo == DOUBLE_PAR){
				code[idx] = 0xff;
                idx++;
				code[idx] = 0x75;
                idx++;
				code[idx] = (unsigned char)ebp;
                idx++;
				ebp = ebp - 4;
			}
		}
		else {
			printf("\nValor para parametro amarrado invalido!\n");
            return 0;
		}
	}
    // call
    code[idx] = 0xe8;
    idx++;
    // fc ff ff ff
    func_adress = (int)f - (int) &code[idx + 4];
	code[idx] = func_adress;
    idx++;
	code[idx] = (func_adress >> 8);
    idx++;
	code[idx] = (func_adress >> 16);
    idx++;
	code[idx] = (func_adress >> 24);
    idx++;
    // mov %ebp, %esp
	code[idx] = 0x89;
    idx++;
	code[idx] = 0xec;
    idx++;
    // pop %ebp
	code[idx] = 0x5d;
    idx++;
    // ret
	code[idx] = 0xc3;
    idx++;
    return code;
}

void libera_func (void* func){
	free (func);
}

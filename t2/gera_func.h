#ifndef SB_T2_Otavio_gera_func_h
#define SB_T2_Otavio_gera_func_h

typedef enum {
    INT_PAR,
    CHAR_PAR,
    DOUBLE_PAR,
    PTR_PAR
} TipoParam;

typedef struct {
    TipoParam tipo;     /* indica o tipo do parametro */
    int amarrado;       /* indica se o parametro deve ter um valor amarrado */
    union {
        int v_int;
        char v_char;
        double v_double;
        void* v_ptr;
    } valor;            /* define o valor do parametro se este for amarrado */
} Parametro;

void* gera_func (void* f, int n, Parametro params[]);

void libera_func (void* func);


#endif

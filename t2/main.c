//
//  main.c
//  SB_T2_Otavio
//
//  Created by Otavio Junqueira on 26/11/12.
//  Copyright (c) 2012 Otavio Junqueira. All rights reserved.
//

/*
 55                       push   %ebp
 89 e5                    mov    %esp,%ebp
 ff 75 08                 pushl  0x8(%ebp)
 e8 fc ff ff ff           call   7 <foo+0x7>
 83 c4 08                 add    $0x8,%esp
 83 c0 05                 add    $0x5,%eax
 89 ec                    mov    %ebp,%esp
 5d                       pop    %ebp
 c3                       ret
*/

#include <stdio.h>
#include <math.h>
#include "gera_func.h"

typedef double (*func_ptr) (double);

double f_soma (double a, double b){
    return a + b;
}

int main(int argc, const char * argv[])
{
    /*
    double x = 2;
    double y = 3;
    double n = pow(x, y);
    printf("pow %.2f e %.2f: %.2f", x, y, n);
    return 0;
    */
    
    Parametro params[2];
    func_ptr f_quadrado = NULL;
    double d;
    double i;
    
    params[0].tipo = DOUBLE_PAR;
    params[0].amarrado = 0;
    params[1].tipo = DOUBLE_PAR;
    params[1].amarrado = 1;
    params[1].valor.v_double = 357;
    
    f_quadrado = (func_ptr) gera_func (f_soma, 2, params);
    
    for (i = 1; i <= 10; i++) {
        d = f_quadrado(i);
        printf("%f ^ 2 = %f\n", i, d);
    }
    
    libera_func(f_quadrado);
    return 0;
}

